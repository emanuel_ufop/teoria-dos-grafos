#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <time.h>

using namespace std;

bool existe_aresta (long**, int);
void conferir (long**, int, list<int>&, list<int>&);
int achar_cliques (string, string);

int achar_cliques(string entrada, string saida)
{
  long **matriz, n_clique = 0;
  char espaco = ' ', bn = '\n', e, edge[5];
 	int vertice, a1, a2;
  long aresta;
  FILE *arq, *arq2;

  // abertura do arquivo
  if((arq = fopen(entrada.data(), "r")) == NULL)
  {
    cout << "Falha ao abrir o arquivo" << endl;
    return 0;
  }

  if((arq2 = fopen(saida.data(), "w")) == NULL)
  {
    cout << "Falha ao criar arquivo de saida" << endl;
    return 0;
  }

  // pegando informacoes sobre o grafo
  fscanf(arq, "%c %s %d %ld \n", &e, edge, &vertice, &aresta);

  // alocação e inicialização de matriz
  matriz = new long*[vertice];
  for(int i = 0; i < vertice; i++)
    matriz[i] = new long[vertice];

  for(int i = 0; i < vertice; i++)
    for(int j = 0; j < vertice; j++)
      matriz[i][j] = 0;

  // insere arestas na matriz
  for(long i = 0; i < aresta; i++)
  {
  	fscanf(arq, "%c %d %d \n", &e, &a1, &a2);
  	matriz[a1 - 1][a2 - 1]++;
  	matriz[a2 - 1][a1 - 1]++;
  }

  list<int> analisar, clique;

  analisar.clear();
  clique.clear();

  // variáveis para cálculo do tempo
  clock_t Ticks[2];
  Ticks[0] = clock();

  do
  {
    for(int i = 0; i < vertice; i++)
    {
    	/*cout << "   ";
  		for(int i = 1; i <= vertice; i++)
  			cout << " " << i;
  	cout << endl;
  	for(int i = 0; i < vertice; i++)
		{
			cout << " " << i + 1 << " ";
   		for(int j = 0; j < vertice; j++) 
    		cout << " " << matriz[i][j];
    	cout << endl;
		}
		getchar();*/

      clique.push_back(i); // vai analisar um clique apartir do vertice i

      for(int j = 0; j < vertice; j++)
        if(matriz[i][j] > 0) // existe aresta entre i e j
          analisar.push_back(j); // coloca j como um dos possíveis elementos de um clique

      if(analisar.size() == 0)
      {
      	clique.clear();
      	continue;
      }

      conferir(matriz, vertice, analisar, clique);

      for(list<int>::iterator it = clique.begin(); it != clique.end(); it++)
      {
        fprintf(arq2, "%-4d", *it + 1); putc(espaco, arq2);
        for(list<int>::iterator it2 = it; it2 != clique.end(); ++it2)
        {
          // remove as arestas entre todos os vertices que estao na lista clique
          matriz[*it][*it2]--;	
          matriz[*it2][*it]--;
        }
      }

      putc(bn, arq2);

      // deletar as listas
      analisar.clear();
      clique.clear();

      n_clique++;
    }
  }while(existe_aresta(matriz, vertice));

  Ticks[1] = clock();
  double Tempo = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;

  fclose(arq);
  fclose(arq2);

  cout << "Numero de cliques: " << n_clique << "   Tempo: " << Tempo << " ms" << endl;

  for(int i = 0; i < vertice; i++)
  	delete matriz[i];

  delete matriz;

  return 1;
 }

bool existe_aresta (long **matriz, int vertice)
{
  for(int i = 0; i < vertice; i++)
      if(matriz[i][0] > 0)
        return true;

  return false; 
}

void conferir (long **matriz, int vertice, list<int> &analisar, list<int> &clique)
{
	bool adjacente;

	for(list<int>::iterator it = analisar.begin(); it != analisar.end(); it++) // anda por toda lista de vertices a serem analisados
	{
		adjacente  = true;
  	for(list<int>::iterator it2 = clique.begin(); it2 != clique.end(); it2++) // anda por toda lista de clique
  	{
  		if(matriz[*it][*it2] == 0) // compara se existe aresta entre os ertices da lista de vertices a serem analisados e vertices que já fazem parte do clique
  		{
  			adjacente = false; // indica que o vertice não é adjacente a pelo menos um dos vértices do clique
  			break; // para o loop, o subgrafo tem que ser completo
  		}
  	}
  	if(adjacente) // se o vertice é adjacente a todos os outros que já estão no clique
  			clique.push_back(*it);
	}
}

int main(int argc, char **argv)
{

	achar_cliques(argv[1], argv[2]);

	return 0;
}